const { Router } = require('express');
const router = Router();
const shipsController = require('../controllers/ships')


router.get('/', shipsController.getShips)
router.get('/add', shipsController.addFormShip)
router.post('/add', shipsController.addShip)
router.get('/delete/:id', shipsController.deleteShip)

router.get('/:id', shipsController.findById)


module.exports = router;
