const { Router } = require('express');
const router = Router();
const pirateController = require('../controllers/pirates')

router.get('/', pirateController.getPirates)
router.get('/add', pirateController.addFormPirate)
router.post('/add', pirateController.addPirate)
router.get('/delete/:id', pirateController.deletePirate)

router.get('/:id', pirateController.findById)

module.exports = router;
