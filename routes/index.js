const { Router } = require('express');
const router = Router();
const shipsRoutes = require('./ships');
const piratesRoutes = require('./pirates');

router.get('/', (req, res) => {
    res.render('index.ejs')
})

router.use('/ships', shipsRoutes);
router.use('/pirates', piratesRoutes);

module.exports = router;


