'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Pirates extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Pirates.init({
    name: DataTypes.CHAR,
    status: DataTypes.CHAR,
    haki: DataTypes.CHAR
  }, {
    sequelize,
    modelName: 'Pirates',
  });
  return Pirates;
};