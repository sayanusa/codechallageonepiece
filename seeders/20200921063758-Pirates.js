'use strict';
const fs = require('fs');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const parseData = JSON.parse(fs.readFileSync('./pirates.json'));
    const piratesData = [];
 
    parseData.forEach(data => {  
     const { name, status, haki } = data;
     piratesData.push({
       name,
       status,
       haki,
       createdAt : new Date(),
       updatedAt : new Date()
     })
   })
    await queryInterface.bulkInsert('Pirates', piratesData, {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Pirates', null, {});
  }
};
